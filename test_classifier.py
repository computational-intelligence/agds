from agds.root import Root
import pandas as pd

def to_val_dict(val_vector):
  val_names = ["leaf-length", "leaf-width", "petal-length", "petal-width"]
  return dict(zip(val_names, val_vector))

if __name__ == '__main__':
  df = pd.read_csv("../data/iris/IrisDataTrain.csv")
  root = Root(df)

  vector = [5.50,2.50,4.00,1.30]
  print(root.classify(to_val_dict(vector)))
