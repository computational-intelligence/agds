from unittest import TestCase
from agds.value_node import ValueNode
from unittest.mock import Mock

VALUE = 42
WEIGHT = 1

class ValueNodeTest(TestCase):
    def setUp(self):
        self.param_node = Mock(value_range=1)
        self.node = ValueNode(VALUE, self.param_node)

        self.other_node = ValueNode(VALUE + 1, self.param_node)
        self.node.connect_value(self.other_node)

        self.object_node = Mock()
        self.node.connect_object(self.object_node)

    def test_init(self):
        self.assertEqual(self.node.value, VALUE)
        self.assertEqual(self.node.param, self.param_node)

    def test_connect_object(self):
        self.assertEqual(self.node.objects[0], self.object_node)
        self.object_node.connect_value.assert_called_once_with(self.node)

    def test_connect_node(self):
        self.assertEqual(self.node.next, self.other_node)
        self.assertEqual(self.other_node.previous, self.node)

    def test_weight(self):
        self.assertEqual(self.node.weight(self.object_node), 1)
        self.assertEqual(self.node.weight(self.other_node), 0)
