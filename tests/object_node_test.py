from unittest import TestCase
from agds.object_node import ObjectNode
from unittest.mock import Mock, call

class ObjectNodeTest(TestCase):
    def test_init(self):
        row = {'a': 1, 'b': 2, 'c': 3}
        root_mock = Mock()
        node = ObjectNode(1, row, root_mock)
        param_calls = [call(p) for p in row.keys()]
        value_calls = [call().get_value(v) for v in row.values()]
        all_calls = [item for pair in zip(param_calls, value_calls) for item in pair]
        root_mock.get_param.assert_has_calls(all_calls)

