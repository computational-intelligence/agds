from unittest import TestCase
from unittest.mock import Mock
from agds.root import Root

class RootTest(TestCase):
    def setUp(self):
        self.df = Mock()
        self.df.items.return_value = {'a': [1, 2, 3], 'b': [4, 5, 6]}.items()
        self.df.iterrows.return_value = []
            # 0: {'a': 1, 'b': 4},
            # 1: {'a': 2, 'b': 5},
            # 2: {'a': 3, 'b': 6}}.items()
        self.root = Root(self.df)

    def test_init(self):
        self.assertEqual(self.root.label, "param")
        self.assertEqual([x.label for x in self.root.params], ['a', 'b'])

    def test_get_param(self):
        self.assertEqual(self.root._get_param("a"), self.root.params[0])
