from unittest import TestCase
from agds.param_node import ParamNode
from unittest.mock import Mock, call, patch

LABEL = "sle"

class ParamNodeTest(TestCase):
    def setUp(self):
        vector = [3,1,2,1]
        self.node = ParamNode(LABEL, vector)

    def test_init(self):
        self.assertEqual(len(self.node.values), 3)
        self.assertEqual(self.node.values[0].next, self.node.values[1])
        self.assertEqual(self.node.values[2].previous, self.node.values[1])
    
    def test_get_value(self):
        self.assertEqual(self.node.get_value(1), self.node.values[0])

