class Node:
  def __init__(self, label):
    self.label = label

  def __lt__(self, other):
    return (self.label < other.label)

  def __str__(self):
    return self.label
