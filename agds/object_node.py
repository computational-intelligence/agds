from .node import Node

class ObjectNode(Node):
    def __init__(self, index, row, root):
        super().__init__(f"R{index + 1}")
        self.values = [root.get_param(p).get_value(v) for p, v in row.items()]
        self.similarity = 0
        for value in self.values:
            value.connect_object(self)

    def clazz(self):
        return next(x.value for x in self.values if x.param.is_class)

    def weight(self, node):
        return 1

    def assoc_inference(self, value, sender = None):
        self.similarity = value
        for val in self.values:
            val.assoc_inference(value * self.weight(val), self)

    def __str__(self):
        return self.label
