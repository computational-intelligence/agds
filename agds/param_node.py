from .value_node import ValueNode
from .node import Node

CLASS_KEYWORD = "class"

class ParamNode(Node):
    def __init__(self, label, vector, root):
        super().__init__(label)
        self.root = root
        self.values = []
        self.is_class = label == CLASS_KEYWORD

        values = list(sorted(set(vector)))
        if not self.is_class:
            self.value_range = max(values) - min(values)
        for value in values:
            self.connect_value(ValueNode(value, self))

    def connect_value(self, value_node):
        if len(self.values) > 0 and not self.is_class:
            value_node.connect_value(self.values[-1])
        self.values.append(value_node)

    def get_value(self, value):
        return next(x for x in self.values if x.value == value)

    def get_nearest(self, value):
        return min(self.values, key=lambda x: abs(x.value - value))

    def __str__(self):
        return self.label
