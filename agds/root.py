from .node import Node
from .param_node import ParamNode
from .object_node import ObjectNode
from operator import itemgetter

class Root(Node):
    def __init__(self, dataframe):
        super().__init__("param")
        self.params = []
        self.objects = []

        for label, column in dataframe.items():
            self.params.append(ParamNode(label, column, self))
        for index, row in dataframe.iterrows():
            self.objects.append(ObjectNode(index, row, self))

    def similar_to_object(self, index):
        self._reset()
        obj = self._get_object(index)
        obj.assoc_inference(1)
        return self._sorted_objects()

    def similar_to_values(self, value_dict):
        self._reset()
        for obj in self.objects:
            obj.similarity = 0
        value_nodes = [self.get_param(k).get_nearest(v) for k, v in value_dict.items()]
        for node in value_nodes:
            node.assoc_inference(1)
        return self._sorted_objects()

    def classify(self, val_dict, take=None):
        objects = self.similar_to_values(val_dict)
        class_ranking = {}
        if take is None:
            take = len(self.objects)

        for clazz, score in [(x.clazz(), x.similarity) for x in objects[:take]]:
            class_ranking[clazz] = class_ranking.get(clazz, 0) + score
        return max(class_ranking.items(), key=itemgetter(1))[0]

    def get_param(self, label):
        return next(x for x in self.params if x.label == label)

    def _get_object(self, index):
        return next(x for x in self.objects if x.label == index)

    def _reset(self):
        for obj in self.objects:
            obj.similarity = 0

    def _sorted_objects(self):
        return sorted(self.objects, key=lambda x: x.similarity, reverse=True)

    def __str__(self):
        return self.label
