from .node import Node

class ValueNode(Node):
    def __init__(self, value, param_node):
        super().__init__(str(value))
        self.value = value
        self.objects = []
        self.param = param_node
        self.similarity = None
        self.next = None
        self.previous = None

    def connect_value(self, value_node):
        if value_node.value > self.value:
            self.next = value_node
            value_node.previous = self
        elif value_node.value < self.value:
            self.previous = value_node
            value_node.next = self
        else:
            raise Exception("Value {} repeated".format(self.value))

    def connect_object(self, object_node):
        self.objects.append(object_node)

    def weight(self, node):
        if type(node) is ValueNode:
            return 1 - abs(self.value - node.value) / self.param.value_range
        else:
            return 1 / len(self.param.root.params)

    def assoc_inference(self, value, sender = None):
        self.similarity = value
        for val in self.neighbors():
            if val is not sender:
                val.assoc_inference(value * self.weight(val), self)
        for obj in self.objects:
            if obj is not sender:
                obj.similarity += value * self.weight(obj)

    def neighbors(self):
        return [x for x in [self.next, self.previous] if x is not None]

    def __str__(self):
        return self.label
