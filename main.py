from agds.root import Root
import pandas as pd

def to_val_dict(val_vector):
  val_names = ["leaf-length", "leaf-width", "petal-length", "petal-width"]
  return dict(zip(val_names, val_vector))

if __name__ == '__main__':
  df = pd.read_csv("../data/iris/IrisData.csv")
  root = Root(df)
  objects = [(x.label, x.similarity) for x in root.similar_to_object(2)]
  print(objects[:5])

  vector = [5.1, 3.5, 1.4, 0.2]
  stv = root.similar_to_values(to_val_dict(vector))
  print([(x.label, x.similarity) for x in stv][:5])

  vector = [5.50,2.50,4.00,1.30]
  print(root.classify(to_val_dict(vector)))
